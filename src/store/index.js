import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
  },
  mutations: {
    increment(state){
      console.log('committed mutation')
      state.counter += 1;
    }
  },
  actions: {
    waitAndIncrement({commit}){
      console.log('dispatched action')
      setTimeout(()=> {
        commit("increment")
      }, 2000)
    }
  },
})
